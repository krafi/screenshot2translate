
import tkinter as tk
from tkinter import ttk

import threading
import os
import datetime
import pytesseract
from PIL import Image
from translate import Translator
import pyautogui
class LanguageConverterGUI:
    def __init__(self, root):
        

        self.root = root
        self.root.title("Language Converter")

        self.from_lang = tk.StringVar()
        self.to_lang = tk.StringVar()
        self.selected_from_lang = ""
        self.selected_to_lang = ""
        self.pytesseract_lang = ""

        self.lang_language_codes = {
            'Afrikaans': ('af', 'afr'),
            'Amharic': ('am', 'amh'),
            'Arabic': ('ar', 'ara'),
            'Assamese': ('as', 'asm'),
            'Azerbaijani': ('az', 'aze'),
            'Azerbaijani (Cyrillic)': ('az', 'aze_cyrl'),
            'Belarusian': ('be', 'bel'),
            'Bengali': ('bn', 'ben'),
            'Tibetan': ('bo', 'bod'),
            'Bosnian': ('bs', 'bos'),
            'Breton': ('br', 'bre'),
            'Bulgarian': ('bg', 'bul'),
            'Catalan': ('ca', 'cat'),
            'Cebuano': ('ceb', 'ceb'),
            'Czech': ('cs', 'ces'),
            'Chinese (Simplified)': ('zh-CN', 'chi_sim'),
            'Chinese (Traditional)': ('zh-TW', 'chi_tra'),
            'Cherokee': ('chr', 'chr'),
            'Welsh': ('cy', 'cym'),
            'Danish': ('da', 'dan'),
            'German': ('de', 'deu'),
            'Divehi': ('dv', 'div'),
            'Dzongkha': ('dz', 'dzo'),
            'Greek': ('el', 'ell'),
            'English': ('en', 'eng'),
            'Middle English': ('enm', 'enm'),
            'Esperanto': ('eo', 'epo'),
            'Estonian': ('et', 'est'),
            'Basque': ('eu', 'eus'),
            'Faroese': ('fo', 'fao'),
            'Persian': ('fa', 'fas'),
            'Finnish': ('fi', 'fin'),
            'French': ('fr', 'fra'),
            'Frankish': ('frk', 'frk'),
            'Middle French': ('frm', 'frm'),
            'Frisian': ('fy', 'fry'),
            'Scottish Gaelic': ('gd', 'gla'),
            'Irish': ('ga', 'gle'),
            'Galician': ('gl', 'glg'),
            'Ancient Greek': ('grc', 'grc'),
            'Gujarati': ('gu', 'guj'),
            'Haitian Creole': ('ht', 'hat'),
            'Hebrew': ('he', 'heb'),
            'Hindi': ('hi', 'hin'),
            'Croatian': ('hr', 'hrv'),
            'Hungarian': ('hu', 'hun'),
            'Inuktitut': ('iu', 'iku'),
            'Indonesian': ('id', 'ind'),
            'Icelandic': ('is', 'isl'),
            'Italian': ('it', 'ita'),
            'Old Italian': ('it', 'ita_old'),
            'Javanese': ('jv', 'jav'),
            'Japanese': ('ja', 'jpn'),
            'Kannada': ('kn', 'kan'),
            'Georgian': ('ka', 'kat'),
            'Old Georgian': ('ka', 'kat_old'),
            'Kazakh': ('kk', 'kaz'),
            'Khmer': ('km', 'khm'),
            'Kirghiz': ('ky', 'kir'),
            'Korean': ('ko', 'kor'),
            'Kurdish': ('ku', 'kur'),
            'Lao': ('lo', 'lao'),
            'Latin': ('la', 'lat'),
            'Latvian': ('lv', 'lav'),
            'Lithuanian': ('lt', 'lit'),
            'Luxembourgish': ('lb', 'ltz'),
            'Malayalam': ('ml', 'mal'),
            'Marathi': ('mr', 'mar'),
            'Macedonian': ('mk', 'mkd'),
            'Maltese': ('mt', 'mlt'),
            'Mongolian': ('mn', 'mon'),
            'Maori': ('mi', 'mri'),
            'Malay': ('ms', 'msa'),
            'Burmese': ('my', 'mya'),
            'Nepali': ('ne', 'nep'),
            'Dutch': ('nl', 'nld'),
            'Norwegian Bokmal': ('nb', 'nob'),
            'Norwegian Nynorsk': ('nn', 'nno'),
            'Norwegian': ('no', 'nor'),
            'Occitan': ('oc', 'oci'),
            'Oriya': ('or', 'ori'),
            'Punjabi': ('pa', 'pan'),
            'Polish': ('pl', 'pol'),
            'Portuguese': ('pt', 'por'),
            'Pashto': ('ps', 'pus'),
            'Quechua': ('qu', 'que'),
            'Romanian': ('ro', 'ron'),
            'Russian': ('ru', 'rus'),
            'Sanskrit': ('sa', 'san'),
            'Sinhala': ('si', 'sin'),
            'Slovak': ('sk', 'slk'),
            'Slovenian': ('sl', 'slv'),
            'Sindhi': ('sd', 'snd'),
            'Spanish': ('es', 'spa'),
            'Old Spanish': ('es', 'spa_old'),
            'Albanian': ('sq', 'sqi'),
            'Serbian': ('sr', 'srp'),
            'Serbian (Latin)': ('sr', 'srp_latn'),
            'Sundanese': ('su', 'sun'),
            'Swahili': ('sw', 'swa'),
            'Swedish': ('sv', 'swe'),
            'Syriac': ('syr', 'syr'),
            'Tamil': ('ta', 'tam'),
            'Telugu': ('te', 'tel'),
            'Tajik': ('tg', 'tgk'),
            'Tagalog': ('tl', 'tgl'),
            'Thai': ('th', 'tha'),
            'Tigrinya': ('ti', 'tir'),
            'Tongan': ('to', 'ton'),
            'Turkish': ('tr', 'tur'),
            'Uighur': ('ug', 'uig'),
            'Ukrainian': ('uk', 'ukr'),
            'Urdu': ('ur', 'urd'),
            'Uzbek': ('uz', 'uzb'),
            'Uzbek (Cyrillic)': ('uz', 'uzb_cyrl'),
            'Vietnamese': ('vi', 'vie'),
            'Yiddish': ('yi', 'yid'),
            'Yoruba': ('yo', 'yor'),
            'Chinese': ('zh-CN', 'zho'),
            'Zulu': ('zu', 'zul')
        }

        self.languages = list(self.lang_language_codes.keys())

        self.from_label = ttk.Label(root, text="From Language:")
        self.from_label.grid(row=0, column=0, padx=10, pady=5)
        self.from_dropdown = ttk.Combobox(root, values=self.languages, textvariable=self.from_lang)
        self.from_dropdown.grid(row=0, column=1, padx=10, pady=5)
        self.from_dropdown.bind("<<ComboboxSelected>>", self.update_lang_codes)

        self.to_label = ttk.Label(root, text="To Language:")
        self.to_label.grid(row=1, column=0, padx=10, pady=5)
        self.to_dropdown = ttk.Combobox(root, values=self.languages, textvariable=self.to_lang)
        self.to_dropdown.grid(row=1, column=1, padx=10, pady=5)
        self.to_dropdown.bind("<<ComboboxSelected>>", self.update_lang_codes)

        self.search_label = ttk.Label(root, text="Search:")
        self.search_label.grid(row=2, column=0, padx=10, pady=5)
        self.search_entry = ttk.Entry(root)
        self.search_entry.grid(row=2, column=1, padx=10, pady=5)
        self.search_entry.bind("<KeyRelease>", self.filter_languages)

        self.done_button = ttk.Button(root, text="Done", command=self.print_selected_languages)
        self.done_button.grid(row=3, column=0, columnspan=2, pady=10)

        self.filtered_languages = self.languages

    def print_selected_languages(self):
        selected_from_lang = self.from_lang.get()
        selected_to_lang = self.to_lang.get()
        pytesseract_lang = self.pytesseract_lang
        #app.close_gui()
        root = tk.Tk()
        app = Application(root,selected_from_lang, selected_to_lang, pytesseract_lang )
        root.mainloop()
        self.selected_from_lang = selected_from_lang
        self.selected_to_lang =  selected_to_lang
        print("From Language:", selected_from_lang)
        print("To Language:", selected_to_lang)
        print("Pytesseract Language:", self.pytesseract_lang)


    def update_lang_codes(self, event):
        selected_from_lang = self.from_lang.get()
        selected_to_lang = self.to_lang.get()

        if selected_from_lang in self.lang_language_codes:
            self.pytesseract_lang = self.lang_language_codes[selected_from_lang][1]
            self.from_lang.set(self.lang_language_codes[selected_from_lang][0])

        if selected_to_lang in self.lang_language_codes:
            self.to_lang.set(self.lang_language_codes[selected_to_lang][0])

    def filter_languages(self, event):
        search_query = self.search_entry.get().lower()
        self.filtered_languages = [lang for lang in self.languages if search_query in lang.lower()]
        self.from_dropdown["values"] = self.filtered_languages
        self.to_dropdown["values"] = self.filtered_languages


class Application:
    def __init__(self, master,selected_from_lang, selected_to_lang, pytesseract_lang):
        self.master = master
        self.start_x = None
        self.start_y = None
        self.current_x = None
        self.current_y = None
        self.timer = None

        #self.pytesseract_lang = "rus"
        #self.from_lang="ru"
        #self.to_lang="en"
        self.from_lang = selected_from_lang
        self.to_lang = selected_to_lang
        self.pytesseract_lang = pytesseract_lang
        #print("===========")
       # print(self.from_lang)
        #print(self.to_lang)
        #print(self.pytesseract_lang)
        #print("===========")


        self.master.geometry('1200x600+200+200')
        self.master.title('screenshot2translate')

        self.master_screen = tk.Toplevel(self.master)
        self.master_screen.withdraw()
        self.master_screen.attributes("-transparent", "maroon3")
        self.picture_frame = tk.Frame(self.master_screen, background="maroon3")
        self.picture_frame.pack(fill=tk.BOTH, expand=True)

        self.create_screen_canvas()

        self.result_label = tk.Label(self.picture_frame, text="", fg="white", bg="maroon3")
        self.result_label.pack()

        restart_button = tk.Button(self.master, text="Retry", command=self.restart_program)
        restart_button.pack()

    def create_screen_canvas(self):
        self.master_screen.deiconify()
        self.master.withdraw()

        self.snip_surface = tk.Canvas(self.picture_frame, cursor="cross", bg="grey11")
        self.snip_surface.pack(fill=tk.BOTH, expand=True)

        self.snip_surface.bind("<ButtonPress-1>", self.on_button_press)
        self.snip_surface.bind("<B1-Motion>", self.on_snip_drag)
        self.snip_surface.bind("<ButtonRelease-1>", self.on_button_release)

        self.master_screen.attributes('-fullscreen', True)
        self.master_screen.attributes('-alpha', .299)
        self.master_screen.lift()
        self.master_screen.attributes("-topmost", True)

    def on_button_release(self, event):
        x1, y1, x2, y2 = self.get_coordinates()
        file_name, extracted_text = self.take_bounded_screenshot(x1, y1, x2, y2)
        self.result_label.config(text="Screenshot saved as: " + file_name + ".png")
        self.exit_screenshot_mode_start_another_gui(extracted_text)

    def get_coordinates(self):
        x1 = int(self.start_x)
        y1 = int(self.start_y)
        x2 = int(self.current_x)
        y2 = int(self.current_y)

        if x1 <= x2 and y1 <= y2:
            return x1, y1, x2, y2
        elif x1 >= x2 and y1 <= y2:
            return x2, y1, x1, y2
        elif x1 <= x2 and y1 >= y2:
            return x1, y2, x2, y1
        else:
            return x2, y2, x1, y1

    def exit_screenshot_mode_start_another_gui(self, extracted_text):
        print(extracted_text)
        self.snip_surface.destroy()
        self.master_screen.withdraw()
        self.master.deiconify()

        user_input = tk.Text(self.master, wrap="word")
        user_input.insert("1.0", extracted_text)
        self.user_input = user_input

        self.translator = Translator(to_lang=self.to_lang, from_lang=self.from_lang)
        translated_text = self.translator.translate(extracted_text)

        user_input_translate = tk.Text(self.master, wrap="word")
        user_input_translate.insert("1.0", translated_text)
        self.user_input_translate = user_input_translate

        button_frame = tk.Frame(self.master)

        copy_button = tk.Button(button_frame, text="Copy", command=lambda: self.copy_text(user_input))
        copy_button.pack(side="left")

        kill_button = tk.Button(button_frame, text="close", command=self.exit_program)
        kill_button.pack(side="right")

        button_frame.pack()
        user_input.pack(side="left", fill=tk.BOTH, expand=True)
        user_input_translate.pack(side="right", fill=tk.BOTH, expand=True)

        recheck_translation_button = tk.Button(button_frame, text="recheck_translation",  command=lambda: self.recheck_translation(user_input))
        recheck_translation_button.pack(side="right")
        #self.recheck_translation(user_input)



    def recheck_translation(self, user_input):
        print("Checking translation...")
        print(self.user_input.get("1.0", "end-1c"))
        thetext = (self.user_input.get("1.0", "end-1c"))
        translation = self.translator.translate(thetext)
        self.user_input_translate.delete("1.0", tk.END)
        self.user_input_translate.insert("1.0", translation)
        #self.timer = threading.Timer(2, self.recheck_translation, args=[user_input])
        #self.timer.start()

    def copy_text(self, text_widget):
        text = text_widget.get("1.0", "end-1c")
        self.master.clipboard_clear()
        self.master.clipboard_append(text)

    def exit_program(self):
        self.master.destroy()

    def on_button_press(self, event):
        self.start_x = self.snip_surface.canvasx(event.x)
        self.start_y = self.snip_surface.canvasy(event.y)
    def on_snip_drag(self, event):
        self.current_x, self.current_y = event.x, event.y
        self.snip_surface.delete("selection_rect")
        self.snip_surface.create_rectangle(self.start_x, self.start_y, self.current_x, self.current_y, outline="", fill="#550000", width=3, tag="selection_rect")


    def take_bounded_screenshot(self, x1, y1, x2, y2):
        directory = os.path.join(os.getcwd(), "snips")
        if not os.path.exists(directory):
            os.makedirs(directory)
        image = pyautogui.screenshot(region=(x1, y1, x2 - x1, y2 - y1))
        file_name = datetime.datetime.now().strftime("%f")
        file_path = os.path.join(directory, file_name + ".png")
        image.save(file_path)

        pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
        extracted_text = pytesseract.image_to_string(Image.open(file_path), lang=self.pytesseract_lang)
        return file_name, extracted_text

    def restart_program(self):
        self.master.destroy()
        #root = tk.Tk()
        #app = Application(root,selected_from_lang, selected_to_lang, pytesseract_lang )
        root = tk.Tk() # (self, master,selected_from_lang, selected_to_lang, pytesseract_lang)
        app = Application(root, self.from_lang , self.to_lang , self.pytesseract_lang)
        root.mainloop()

if __name__ == '__main__':
    root = tk.Tk()
    app = LanguageConverterGUI(root)
    root.mainloop()
    app = Application(root)
    root.mainloop()
