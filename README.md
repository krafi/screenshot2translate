# Installation for Windows

1. Press on the Google Drive link to get the .exe for Windows.https://drive.google.com/drive/folders/1r_lmuG_KfKEpSONNNW6x6prBS8AUXKxp?usp=sharing
2. Download "tesseract-ocr-w64" and "screenshot2translate.exe".
3. Install "tesseract-ocr-w64". During installation, make sure your desired language is selected. You can choose to select all languages if needed.
4. Run "screenshot2translate.exe".

# Language Converter with OCR

This Python application allows users to perform language translation using OCR (Optical Character Recognition). The application provides a graphical user interface (GUI) for selecting input and output languages, capturing screenshots, extracting text using OCR, and translating the extracted text.

## Features

- Select input and output languages from a dropdown menu.
- Capture screenshots of selected regions on the screen.
- Extract text from screenshots using OCR.
- Translate extracted text from one language to another.
- Copy translated text to clipboard.
- Retry functionality to capture new screenshots and translate text again.

## Dependencies

- Python 3.x
- Tkinter
- Pillow
- Pytesseract
- Translate
- PyAutoGUI

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details.
